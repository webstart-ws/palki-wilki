


// menu section
function menuOpen(){
    setTimeout(function(){
      document.getElementById("black_background").classList.add("black_background_active")
      document.getElementById("menu").classList.add("menu_active")
      document.getElementById("body").classList.add("overflow")
   },10);
  };
  function menuClose(){
    document.getElementById("black_background").classList.remove("black_background_active")
    document.getElementById("menu").classList.remove("menu_active")
    document.getElementById("body").classList.remove("overflow")
  };
  
  document.addEventListener('click', function(e) {
      if(!e.target.closest(".menu")){
        var x, y, i;
        var  r = document.querySelectorAll("#body");
        x = document.querySelectorAll(".black_background");
        y = document.querySelectorAll(".menu");
        for (i = 0; i < x.length; i++) {
          x[i].classList.remove('black_background_active');
          y[i].classList.remove('menu_active');
          r[i].classList.remove('overflow');
        }
      }
    });
  // menu section end



// product page special product section
function productOpen(){
    setTimeout(function(){
      document.getElementById("black_background_product").classList.add("black_background_product_active")
      document.getElementById("body").classList.add("overflow")
   },10);
  };
  function productClose(){
    document.getElementById("black_background_product").classList.remove("black_background_product_active")
    document.getElementById("body").classList.remove("overflow")
  };
  
  document.addEventListener('click', function(e) {
      if(!e.target.closest(".preview_product_block")){
        var z, i;
        var  r = document.querySelectorAll("#body");
        z = document.querySelectorAll(".black_background_product");
        for (i = 0; i < z.length; i++) {
          z[i].classList.remove('black_background_product_active');
          r[i].classList.remove('overflow');
        }
      }
    });
  // product page special product section end

// call back section
function call_back_Open(){
    setTimeout(function(){
      document.getElementById("call_back").classList.add("call_back_active")
      document.getElementById("body").classList.add("overflow")
   },10);
  };
  function call_back_Close(){
    document.getElementById("call_back").classList.remove("call_back_active")
    document.getElementById("body").classList.remove("overflow")
  };
  
  document.addEventListener('click', function(e) {
      if(!e.target.closest(".call_back_block")){
        var p, i;
        var  r = document.querySelectorAll("#body");
        p = document.querySelectorAll(".call_back");
        for (i = 0; i < p.length; i++) {
          p[i].classList.remove('call_back_active');
          r[i].classList.remove('overflow');
        }
      }
    });
  // call back end